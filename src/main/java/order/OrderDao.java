package order;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.simpleflatmapper.jdbc.spring.JdbcTemplateMapperFactory;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;


import javax.sql.DataSource;

@Repository
public class OrderDao {

    private JdbcTemplate template;

    private final ResultSetExtractor<List<Order>> mapper =
            JdbcTemplateMapperFactory
                    .newInstance()
                    .addKeys("id")
                    .newResultSetExtractor(Order.class);

    public OrderDao(JdbcTemplate template) {
        this.template = template;
    }

    public Order getById(long id){
        String sql = "select o.id as id, ordernumber, r.id as orderrow_id, " +
                "r.itemname as orderrow_itemname, " +
                "r.price as orderrow_price, " +
                "r.quantity as orderrow_quantity " +
                "from orders o left join orderrow r on r.order_id = o.id where o.id = ?";
        return template.query(sql, new Object[]{id}, mapper).get(0);
    }
    public List<Order> getAll(){
        String sql = "select o.id as id, ordernumber, " +
                "r.id as orderrow_id, " +
                "r.itemname as orderrow_itemname, " +
                "r.price as orderrow_price, " +
                "r.quantity as orderrow_quantity " +
                "from orders o left join orderrow r on r.order_id = o.id order by o.id";
        return template.query(sql, mapper);
    }

    public Order insertOrder(Order order){
        SqlParameterSource data = new BeanPropertySqlParameterSource(order);
        Number orderKey = new SimpleJdbcInsert(template).withTableName("orders")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(data);

        order.setId(orderKey.longValue());

        if (order.getOrderRows() != null) {
            for (OrderRow row : order.getOrderRows()) {
                SqlParameterSource rowData = new BeanPropertySqlParameterSource(row);
                row.setOrderId(orderKey.longValue());
                Number orderRowKey = new SimpleJdbcInsert(template).withTableName("orderrow")
                        .usingGeneratedKeyColumns("id")
                        .executeAndReturnKey(rowData);
                row.setId(orderRowKey.longValue());
            }
        }
        return order;
    }

    public void delete(long deleteId) {
        String sql = "delete from orders where id = ?";
        template.update(sql, deleteId);
    }
}
