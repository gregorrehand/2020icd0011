package order;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
public class Order {
    private Long id;
    @Size(min=2)
    private String orderNumber;
    @Valid
    private List<OrderRow> orderRows;
    public Order() {

    }

    public Order(Long id, String orderNumber, List<OrderRow>  orderRows) {
        this.id = id;
        this.orderNumber = orderNumber;
        this.orderRows = orderRows;
    }

    public List<OrderRow>  getOrderRows() {
        return orderRows;
    }

    public void setOrderRows(List<OrderRow>  orderRows) {
        this.orderRows = orderRows;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
}

