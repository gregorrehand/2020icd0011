package order;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {
    private OrderDao orderDao;
    private static final String ROUTESTRING = "orders";

    public OrderController(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @RequestMapping (value=ROUTESTRING, method=RequestMethod.GET)
    public List<Order> getAll() {
        return orderDao.getAll();
    }
    @RequestMapping (value=ROUTESTRING, method=RequestMethod.GET, params={"id"})
    public Order getById(@RequestParam Long id) {
        return orderDao.getById(id);
    }
    @PostMapping(ROUTESTRING)
    public Order createOrder(@RequestBody @Valid Order order) {
        return orderDao.insertOrder(order);
    }
    @DeleteMapping(ROUTESTRING)
    public void deleteOrder(@RequestParam Long id) {
        orderDao.delete(id);
    }
}
