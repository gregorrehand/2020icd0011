CREATE SEQUENCE seq1 START WITH 1;


CREATE TABLE orders (
   id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
   orderNumber VARCHAR(255) NOT NULL,
);

CREATE TABLE orderrow (
   id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
   itemname VARCHAR(255) NOT NULL,
    quantity INT,
    price INT,
    order_id BIGINT NOT NULL references orders(id) ON DELETE CASCADE
);